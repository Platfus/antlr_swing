tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer scope = 0;
}
prog    : (e+=zakres | e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d})
        ;
        
zakres  : ^(BEGIN {scope++; enterScope();} (e+=zakres | e+=expr | d+= decl)* {scope--; leaveScope();}) -> blok(wyr={$e}, dekl={$d})
        ;

decl  :
        ^(VAR i1=ID) {locals.newSymbol($ID.text);} -> dek(name={$ID.text},number={scope.toString()})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> sub(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) -> setValue(p1={locals.getSymbolNameInScope($ID.text)},p2={$e2.st})
        | INT                      -> int(i={$INT.text})
        | ID                       -> id(n={locals.getSymbolNameInScope($ID.text)})
    ;
    